import {
    FAILED_RESPONSE,
    SUCCESS_RESPONSE,
    PENDING_RESPONSE,
    SET_COMPLETE_GAME,
    SET_NEW_GAME
} from '../constants';

export const newGame = () => ({ type: SET_NEW_GAME });
export const success = () => ({ type: SUCCESS_RESPONSE });
export const failed = () => ({ type: FAILED_RESPONSE });
export const pending = () => ({ type: PENDING_RESPONSE });
export const completeGame = () => ({ type: SET_COMPLETE_GAME });