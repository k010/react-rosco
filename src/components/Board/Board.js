import React from 'react';
import classNames from 'classnames';
import './styles.css';
import Menu from '../Menu/Menu';
import useRosco from '../../hooks/useRosco';
import { completeGame, success, pending, failed, newGame } from '../../actions/rosco';
import Rosco from '../Rosco';
import { PENDING_STATE, EMPTY_STATE, SUCCESS_STATE } from '../../constants';

const Board = () => {
    const [state, dispatch] = useRosco();
    const handleOnNewGame = () => dispatch(newGame());
    const counter = state.game.reduce((prev, curr) => curr === SUCCESS_STATE ? prev + 1 : prev, 0);
    function handleResponseOk() {
        dispatch(success());
    }

    function handleResponseFailed() {
        dispatch(failed());
    }

    function handleResponsePending() {
        dispatch(pending());
    }

    const checkGameStatus = () => {
        const isDone = state.game.every(pos => !(pos === PENDING_STATE || pos === EMPTY_STATE));

        if (isDone && !state.isComplete) {
            dispatch(completeGame(state));
        }
    }

    checkGameStatus();

    return (
        <section className="board">
            <div className="menu">
                <Menu
                    onFailed={ handleResponseFailed }
                    onSuccess={ handleResponseOk }
                    onPending={ handleResponsePending }
                    onNewGame={ handleOnNewGame }
                    showNewGame={ state.isComplete }
                />
                <div className={ classNames('counter') }>{`${counter}`}</div>
            </div>
            <div className="rosco">
                <Rosco game={ state.game } rosco={state.rosco} position={state.position} />
            </div>
        </section>
    );
};

export default Board;
