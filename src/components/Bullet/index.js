import React, { useRef, useEffect } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { SUCCESS_STATE, FAILED_STATE, PENDING_STATE } from '../../constants';
import './styles.css';

const MAX_SIZE = 500;
const getComputedStyle = (width, length, current = 0) => {
    const angle = 360 / length;
    const rotation = (current * angle);

    return `transform: rotate(${rotation}deg) translate(${width / 2}px) rotate(-${rotation}deg)`;
};

const Bullet = ({ letter, status, current, index, parentSize, bullets }) => {
    const domEl = useRef();

    useEffect(() => {
        const size = parentSize > MAX_SIZE ? MAX_SIZE : parentSize;
        const style = getComputedStyle(size - domEl.current.offsetWidth, bullets, index);

        domEl.current.style = style;
    }, [parentSize]);

    return (
        <div
            ref={domEl}
            className={classnames(
                'bullet',
                {
                    ok: status === SUCCESS_STATE,
                    error: status === FAILED_STATE,
                    pending: status === PENDING_STATE,
                    current
                }
            )}
        >
            <span>{ letter.toUpperCase() }</span>
        </div>
    )
};

Bullet.propTypes = {
    letter: PropTypes.string.isRequired,
    status: PropTypes.number.isRequired
};

export default Bullet;
