import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './styles.css';

const Menu = ({ onSuccess, onFailed, onPending, showNewGame, onNewGame }) => {
    return (
        <nav className={classNames("navigation")}>
            { !showNewGame && (
                <React.Fragment>
                    <div className={classNames("action")}>
                        <button onClick={ onSuccess }>OK</button>
                    </div>
                    <div className={classNames("action")}>
                        <button onClick={ onFailed }>FALLO</button>
                    </div>
                    <div className={classNames("action")}>
                        <button onClick={ onPending }>PASA PALABRA</button>
                    </div>
                </React.Fragment>
            )}
            { showNewGame && (
                <div className={classNames("action")}>
                    <button onClick={ onNewGame }>NUEVO JUEGO</button>
                </div>
            ) }
        </nav>
    );
};

Menu.propTypes = {
    showNewGame: PropTypes.bool.isRequired
};

export default Menu;
