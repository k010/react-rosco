import React, { useRef } from 'react';
import classNames from 'classnames';
import Bullet from '../Bullet';
import useElementClientSize from '../../hooks/useElementClientSize';
import './styles.css';

const Rosco = ({ rosco, game, position }) => {
    const containerEl = useRef();
    const { width, height } = useElementClientSize(containerEl);
    const smallerSize = width < height ? width : height;
    const style = { width: smallerSize, height: smallerSize };

    return (
        <div className={ classNames('roscoContainer') } ref={containerEl}>
            <div className={ classNames('bulletContainer') } style={ style }>
                {
                    rosco.map((letter, index) => (
                        <Bullet
                            key={ `${letter}_${index}` }
                            letter={ letter }
                            status={ game[index] }
                            current={ (position === index) }
                            index={ index }
                            parentSize={ smallerSize }
                            bullets={ game.length }
                        />
                    ))
                }
            </div>
        </div>
    );
};

export default Rosco;
