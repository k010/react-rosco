import { PENDING_STATE, INVALID_STATE, EMPTY_STATE } from '../constants';

const LETTERS = 'vendormanagement';
const findRecursive = (game, position = 0) => {
    if (position > game.length) {
        return INVALID_STATE;
    }

    if (game[position] === PENDING_STATE) {
        return position;
    }

    if (game[position] === EMPTY_STATE) {
        return position;
    }

    return findRecursive(game, position + 1);
};

export const getGameLetters = () => LETTERS.split('');

export const setGameValue = (game, value, position) =>
    [...game.slice(0, position), value, ...game.slice(position + 1)];

export const nextPendingPosition = (game, position) => {
    const find = findRecursive(game, position);
    if (find !== INVALID_STATE) {
        return find;
    }

    return findRecursive(game, 0) !== INVALID_STATE ? findRecursive(game, 0) : position;
};
