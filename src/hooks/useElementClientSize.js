import { useState, useEffect } from 'react';

const defaultState = { width: 0, height: 0 };

export default (domElement, initialState = defaultState) => {
    const [size, setSize] = useState(initialState);
    const handlerSize = () => {
        const { current: { clientWidth, clientHeight }} = domElement;

        setSize({ width: clientWidth, height: clientHeight });
    };

    useEffect(() => {
        handlerSize();
    }, []);
    useEffect(() => {
        window.addEventListener('resize', handlerSize);

        return () => window.removeEventListener('resize', handlerSize);
    }, [size]);

    return size;
}