import { useReducer } from 'react';
import roscoReducer, { initialState } from '../reducers/rosco';

export default () => {
    const [state, dispatch] = useReducer(roscoReducer, initialState);

    return [state, dispatch];
};
