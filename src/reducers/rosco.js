import {
    getGameLetters
} from '../helpers/settings';
import {
    PENDING_STATE,
    FAILED_RESPONSE,
    FAILED_STATE,
    SUCCESS_RESPONSE,
    SUCCESS_STATE,
    PENDING_RESPONSE,
    SET_COMPLETE_GAME,
    SET_NEW_GAME,
    INVALID_STATE,
    EMPTY_STATE
} from '../constants';

import { setGameValue, nextPendingPosition } from '../helpers/settings';

const rosco = getGameLetters();

export const initialState = {
    rosco,
    position: 0,
    game: rosco.map(r => EMPTY_STATE),
    isComplete: false
};

export default (state = initialState, action) => {
    switch (action.type) {
        case FAILED_RESPONSE:
            return {
                ...state,
                game: setGameValue(state.game, FAILED_STATE, state.position),
                position: nextPendingPosition(setGameValue(state.game, FAILED_STATE, state.position), state.position)
            };
        case SUCCESS_RESPONSE:
            return {
                ...state,
                game: setGameValue(state.game, SUCCESS_STATE, state.position),
                position: nextPendingPosition(setGameValue(state.game, SUCCESS_STATE, state.position), state.position)
            };
        case PENDING_RESPONSE:
            return {
                ...state,
                game: setGameValue(state.game, PENDING_STATE, state.position),
                position: nextPendingPosition(setGameValue(state.game, INVALID_STATE, state.position), state.position)
            };
        case SET_COMPLETE_GAME:
            return {
                ...state,
                isComplete: true
            }
        case SET_NEW_GAME: {
            return initialState;
        }
        default:
            return state;
    }
};